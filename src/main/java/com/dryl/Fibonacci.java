package com.dryl;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {
  private int n;
  private List<Integer> list = new ArrayList<>();
  private int result;

  public Fibonacci(int n) {
    this.n = n;
  }
  public void fillList(int n) {
    if (n<=2){
      result=1;
      list.add(result);
    }else {
      Fibonacci f1 = new Fibonacci(n-1);
      Fibonacci f2 = new Fibonacci(n-2);
      result = f1.result+f2.result;
      list.add(result);
    }
  }

  public static void main(String[] args) {
    Fibonacci f = new Fibonacci(5);
    System.out.println(f.list);
  }
}
