package com.dryl;

public class SynchronizeWithOther {
  public void syncWithOneObject(){
    Object obj = new Object();
    Thread t1 = new Thread(()->{
      synchronized (obj){
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println("Thread 1");}
    });
    Thread t2 = new Thread(()->{
      synchronized (obj){
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println("Thread 2");}
    });
    Thread t3 = new Thread(()->{
      synchronized (obj){
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println("Thread 3");}
    });
    t1.start();
    t2.start();
    t3.start();
  }
  public void syncWithThreeObjects(){
    Object obj1 = new Object();
    Object obj2 = new Object();
    Object obj3 = new Object();

    Thread t1 = new Thread(()->{
      synchronized (obj1){
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println("Thread 1");}
    });
    Thread t2 = new Thread(()->{
      synchronized (obj2){
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println("Thread 2");}
    });
    Thread t3 = new Thread(()->{
      synchronized (obj3){
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println("Thread 3");}
    });
    t1.start();
    t2.start();
    t3.start();
  }

  public static void main(String[] args) {
    SynchronizeWithOther s = new SynchronizeWithOther();
    s.syncWithThreeObjects();
  }

}
