package com.dryl;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciNumber {

  private int n;
  private int first = 1;
  private int second = 1;
  private int result;
  private Runnable runnable = new Runnable() {
    @Override
    public void run() {
      System.out.print(first + " " + second);
      for (int i = 2; i < n; i++) {
        result = first + second;
        System.out.print(" " + result);
        first = second;
        second = result;
    }
  }};

  public FibonacciNumber(int n) {
    this.n = n;
  }

  public void lineFibo() {
    Thread thread = new Thread(runnable);
    thread.start();
  }

  public static void main(String[] args) throws Exception {
    FibonacciNumber f = new FibonacciNumber(10);
    f.count();
    }
  public void withExecutor() {
    ExecutorService executor = Executors.newSingleThreadExecutor();
    executor.execute(runnable);
    executor.shutdown();
  }
  public void count() throws Exception {
    ExecutorService ex = Executors.newWorkStealingPool();
    Callable<Integer> callable = ()->{
      int res = 0;
      for (int i = 2; i < n; i++) {
        res = first + second;
        first = second;
        second = res;
      }
      return res;
    };
    System.out.println(callable.call());
  }
}
