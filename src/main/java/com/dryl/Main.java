package com.dryl;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Main {

  public static void main(String[] args) {
    Fibonacci f = new Fibonacci(5);
  }

  public void show() {
    Thread lambdaThread = new Thread(
        ()-> {System.out.println("Good, "+Thread.currentThread().getName());
    try {
      Thread.sleep(4000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }},"Lambda Thread");
    lambdaThread.start();
    System.out.println(LocalDateTime.now());
    try {
      lambdaThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(LocalDateTime.now());
    System.out.println("End");
  }
}
