package com.dryl;

public class PingPong {

  private final static Object obj = new Object();

  public void start() {
    Thread t1 = new Thread(
        () -> {
          synchronized (obj) {
            for (int i = 0; i < 10; i++) {
              try {
                obj.wait();
              } catch (InterruptedException e) {
              }
              System.out.print("ping-");
              obj.notify();
            }
          }
        });
    Thread t2 = new Thread(
        () -> {
          synchronized (obj) {
            for (int i = 0; i < 10; i++) {
              obj.notify();
              try {
                obj.wait();
              } catch (InterruptedException e) {}
              System.out.print("pong ");
            }
          }
        }
    );
    t1.start();
    t2.start();
  }
}
