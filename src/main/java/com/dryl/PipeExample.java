package com.dryl;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class PipeExample {

  public static void main(String[] args) throws IOException, InterruptedException {
    String s = "Hello world";
    PipedOutputStream output = new PipedOutputStream();
    PipedInputStream input = new PipedInputStream();
    output.connect(input);
    Thread thread1  = new Thread(()->{
      try {
        output.write(s.getBytes());
        output.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
    Thread thread2 = new Thread(()->{
      try {
        int data = input.read();
        while (data!=-1){
          System.out.println((char) data);
          data = input.read();
        }
        input.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
  thread1.start();
  thread2.start();
  }
}
