package com.dryl;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class SleepWithSchedule {
  public void sleepAndWakeUp(){
    ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
    int random = new Random().nextInt(10);
    Runnable runnable = ()-> System.out.println(random);
    ScheduledFuture<?>future = executorService.schedule(runnable,random,TimeUnit.SECONDS);
    executorService.shutdown();
  }

  public static void main(String[] args) {
    SleepWithSchedule s = new SleepWithSchedule();
    s.sleepAndWakeUp();
  }
}
